use FulfillSaddle;

DROP TABLE IF EXISTS `FulfillSaddle`;
CREATE TABLE `FulfillSaddle` (
  `SaddleId`	varchar(255) NOT NULL,
  `DeptNo`		bigint(20),
  PRIMARY KEY (`SaddleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
