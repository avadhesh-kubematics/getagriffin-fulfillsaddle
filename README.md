# GetAGriffin-FulfillSaddle Component (Microservice)

## Other Repos Needed

The following repo is needed for this product. It is not in Maven Central, so you will need
to clone it and build it, by running `make install`, which will build it and install it
in your local maven repository:

https://gitlab.com/cliffbdf/utilities-java

## To Deploy This Component

### Locally


### In AWS


## To Run Test Suites


### Locally

### Under Jenkins


## Component Level Test Strategy

1. API test

## External Packages used

The microservices in this product use the SparkJava framework, which is a much
lighterweight, less opinionated alternative to Spring. It is also easier for those
new to the framework to understand the code. The documentation for SparkJava
can be found here: http://sparkjava.com/documentation

## The Service's REST api

RequisitionFromInventory?DeptNo=

AddToInventory?SaddleId=

CancelRequisition?SaddleId=
