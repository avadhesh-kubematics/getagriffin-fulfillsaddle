

.PHONY: jar

.DEFAULT_GOAL: build

# Run maven plugins individually so that they don't call the Surefire plugin
build:
	mvn compiler:compile jar:jar install:install
