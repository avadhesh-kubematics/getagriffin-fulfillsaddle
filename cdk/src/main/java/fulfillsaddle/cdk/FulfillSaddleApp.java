package fulfillsaddle.cdk;

import software.amazon.awscdk.core.App;

import java.util.Arrays;

public class FulfillSaddleApp {
    public static void main(final String[] args) {

        App app = new App();
        new FulfillSaddleStack(app, "FulfillSaddleStack");
        app.synth();
    }
}
